function AddDragLine() {
    
    
    plot_area.append("rect")
	.attr("width", 2)
	.attr("height", height)
	.attr("stroke", "black")
	.attr("x", xscale(middle_date))
	.attr("y", 0)
	.style('cursor','col-resize')
	.call(onDrag(dragHandler, dropHandler));
}

function onDrag() {
    var drag = d3.behavior.drag();
    
    drag.on("drag", dragHandler)
	    .on("dragend", dropHandler);
    return drag;
}

function dropHandler(d) {
    middle_date = xscale.invert(this.getAttribute("x")).clearTime( ) ;
    set_xs();
    this.setAttribute("x", xscale(middle_date));
}

function dragHandler(d) {
    if (xscale.invert(d3.event.x) < min_drag_date) {
      d3.select(this).attr("x", xscale(min_drag_date));
    } else if (d3.event.x > width) {
      d3.select(this).attr("x", width);
    } else {
      d3.select(this).attr("x", d3.event.x);
	}
}
