    var margin = {top: 0, right: 0, bottom: 0, left: 0};
    var total_width = 300;
    var total_height = 200;
    var left_fraction = 0.1
    var width = total_width - margin.left - margin.right;
    var height = total_height - margin.top - margin.bottom;
    var scale_height = 30;
    var scale_width = 30;
    var start_date = new Date(2015,8,12);
    var end_date = new Date(2015,8,25);
    var diff_date = { days: -7};
    var middle_date = end_date.clone().add(diff_date)
    var min_drag_date = new Date(start_date.getTime() + (end_date.getTime() - start_date.getTime()) * left_fraction);
     var xscale = d3.time.scale()
                  .domain([start_date, middle_date, end_date])
                  .range([0,left_fraction*width, width]);
     var xAxis = d3.svg.axis();
 
     var yscale = d3.scale.linear()
                  .domain([50,30])
                  .range([0,height]); 
                
     var yAxis = d3.svg.axis()
                  . scale(yscale)
                  .orient("left");
$( document ).ready(function() { 
  $(".plot").each(function(){
    var plot_area = d3.select(this)
    .append("svg")
    .attr("width", total_width)
    .attr("height", total_height)
    .append("g")
    .classed("plot_area", true)
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


                 
    parseDate = d3.time.format("%Y%m%dT%H%MZ").parse
 
    d3.csv(this.getAttribute("href"), function(data) {
       var sqPoint = plot_area.selectAll("circle")
                              .data(data)
                              .enter()
                              .append("svg:a")
                              .attr("xlink:href", function(d){return d["url"];}) 
                              .append("circle")
                              .attr("cy", function(d) {
                                  return yscale(d["value"]); 
                                  })
                              .attr("r", 2);
       $('circle').tipsy({ 
                         gravity: 'w', 
                         html: true, 
                         title: function() {
                                    var d = this.__data__;
                                    return d["hover"]; 
                                    }
                          });
      set_xs();
    });
    var gXAxis = plot_area.append("g").attr("class", "axis").attr("transform", "translate(0," + height + ")");
    
    //plot_area.append("g")
    //.attr("class", "axis")
    //.call(yAxis);
    
    plot_area.append("rect")
           .classed("border", true)
	       .attr("width", 1)
	       .attr("shape-rendering", "crispEdges")
	       .attr("height", height)
	       .attr("stroke", "black")
	       .attr("x", 0)
	       .attr("y", 0);
    plot_area.append("rect")
           .classed("border", true)
	       .attr("width", width)
	       .attr("shape-rendering", "crispEdges")
	       .attr("height", 1)
	       .attr("stroke", "black")
	       .attr("x", 0)
	       .attr("y", 0);    
    plot_area.append("rect")
           .classed("border", true)
	       .attr("width", 1)
	       .attr("shape-rendering", "crispEdges")
	       .attr("height", height)
	       .attr("stroke", "black")
	       .attr("x", width)
	       .attr("y", 0);
    plot_area.append("rect")
           .classed("border", true)
	       .attr("width", width)
	       .attr("shape-rendering", "crispEdges")
	       .attr("height", 1)
	       .attr("stroke", "black")
	       .attr("x", 0)
	       .attr("y", height);
    
    AddDragLine(plot_area);
    });
    
  $(".xscale").each(function(){
    var x_scale_area = d3.select(this)
    .append("svg")
    .attr("width", total_width)
    .attr("height", scale_height)
    .append("g")
    .classed("x_scale_area", true)
    x_scale_area.append("text")
             .attr("x", total_width)
             .attr("y", scale_height * 0.5)
             .style("font-size","14px")
             .attr("text-anchor", "end")
             .text(end_date.toString("dd-MM-yy"));
  });      
  
  $(".yscale").each(function(){
    var y_scale_area = d3.select(this)
    .append("svg")
    .attr("width", scale_width)
    .attr("height", total_height)
    .append("g")
    .classed("y_scale_area", true);
    y_scale_area.append("text")
             .attr("x", scale_width / 2)
             .attr("y", total_height * 0.1)
             .style("font-size","20px")
             .style("text-anchor","middle")
             .text("+");
    y_scale_area.append("text")
             .attr("x", scale_width / 2)
             .attr("y", total_height * 0.3)
             .style("font-size","14px")
             .style("text-anchor","middle")
             .text("+");
    y_scale_area.append("text")
             .attr("x", scale_width / 2)
             .attr("y", total_height * 0.7)
             .style("font-size","14px")
             .style("text-anchor","middle")
             .text("-");
    y_scale_area.append("text")
             .attr("x", scale_width / 2)
             .attr("y", total_height * 0.9)
             .style("font-size","20px")
             .style("text-anchor","middle")
             .text("-");
  });  
});

function set_xs() {
    xscale = d3.time.scale()
                    .domain([start_date, middle_date, end_date])
                    .range([0,left_fraction*width, width]);
        
    d3.selectAll(".day_tick").remove();
	d3.selectAll(".week_tick").remove();
	d3.selectAll(".middle_date_label").remove();
    $(".plot_area").each(function(){
        var plot_area = d3.select(this)
        plot_area.selectAll("circle").attr("cx", function(d) {
                                                       return xscale(parseDate(d["date"]));
                                                              });
        xAxis.scale(xscale)
             .ticks(3)
             .orient("bottom");
        //gXAxis.call(xAxis);
        
        var tick_line = middle_date.clone().clearTime( ).moveToDayOfWeek(1);
        while (tick_line < end_date) {
           plot_area.append("rect")
           .classed("week_tick", true)
	       .attr("width", 1)
	       .attr("shape-rendering", "crispEdges")
	       .attr("height", height * 0.4)
	       .attr("stroke", "black")
	       .attr("x", xscale(tick_line))
	       .attr("y", height * 0.3);
	       tick_line.addDays(7);
	       }
        
        var tick_line = middle_date.clone().clearTime( ).addDays(1);
        while (tick_line < end_date) {
           plot_area.append("rect")
           .classed("day_tick", true)
	       .attr("width", 1)
	       .attr("shape-rendering", "crispEdges")
	       .attr("height", height * 0.2)
	       .attr("stroke", "black")
	       .attr("x", xscale(tick_line))
	       .attr("y", height * 0.4);
	       tick_line.addDays(1);
	       }
	}); 
	$(".x_scale_area").each(function(){
        var x_scale_area = d3.select(this)
        x_scale_area.append("text")
             .attr("x", left_fraction*width)
             .attr("y", scale_height * 0.5)
             .classed("middle_date_label", true)
             .style("font-size","14px")
             .style("text-anchor","middle")
             .text(middle_date.toString("dd-MM-yy"));
        });      
    }

function AddDragLine(plot_area) {
    
    
    plot_area.append("rect")
	.attr("width", 1)
	.attr("height", height)
	.attr("stroke", "black")
	.attr("x", xscale(middle_date))
	.attr("y", 0)
	.style('cursor','col-resize')
	.call(onDrag(dragHandler, dropHandler));
}

function onDrag() {
    var drag = d3.behavior.drag();
    
    drag.on("drag", dragHandler)
	    .on("dragend", dropHandler);
    return drag;
}

function dropHandler(d) {
    middle_date = xscale.invert(this.getAttribute("x")).clearTime( ) ;
    set_xs();
    this.setAttribute("x", xscale(middle_date));
}

function dragHandler(d) {
    if (xscale.invert(d3.event.x) < min_drag_date) {
      d3.select(this).attr("x", xscale(min_drag_date));
    } else if (d3.event.x > width) {
      d3.select(this).attr("x", width);
    } else {
      d3.select(this).attr("x", d3.event.x);
	}
}
